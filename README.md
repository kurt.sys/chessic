# About Chessic

This is a way of making a kind of music which is driven by very specific rules. The idea came from 'transcribing a chess game in notes'. Chessic is a generalized and formalized version of such a transcription.

Each piece of music can be played and a game (whatever that game would be called) and vice versa. Once the rules are set, one can be derived from the other (meaning, the game that was played can be derived from the music, or the music can be derived from the 'moves' in a game).