# General rules

## Playable notes

Each game has a fixed set of playable notes, configured in a specific way. One can choose the notes and configuration, but during a piece, the notes and configuration can't be changed.

A few examples of 'configurations' are:

- 3x3 square: 

    ![Alt text](3x3 base.png){width=150px}

- pyramid:

    ![Alt text](1x5 pyramid.png){width=150px}

- hexagonal:

    ![Alt text](hexagonal.png){width=150px}


- ... (any other configurations are up to the creativitiy of the composer)


## Groups and turns

Each piece consists of turns. There is no fixed rythm. The progression is defined by turns. A group consists of a number of 'elements' (see below), which link to motifs and optionally instruments, dynamics, articulation etc.

Only 1 element can be played at a time. Groups take turns: every other group can decide to let 1 element play it's motif. If a element has played it's motif, and there is only 1 group, than the same, or another element must play. If there are multiple groups, rules must be set up to take turns. The most simple rule is just taking turns. Other rules for turn rotation may be invented.


## Elements

Each piece has a limited number of elements. Elements are distiguished from each other by:
1. element type (se below)
2. current active note.

Once an element plays a specific note, that notes becomes the active note of that element. An element should at least play the active note until the next turn of that group. Even if the note isn't played anymore, that note is still the active note of that element.

Two elements can not play the same note. If one element plays an active note of another element, the other element is removed and can't play anymore. This is called 'element capturing'.

Advanced pieces can have special rules to bring an element back in the piece by promoting to another piece type. For example, if a piece of type X reaches note E5, than it can promote to another piece.

Whenever an element plays, it should start from it's active note. If the current element doesn't have an active note, rules must be setup for what notes can be played the first time an element plays. In other setups, there's a starting position for elements, meaning they all have an active note, that's not played at the start of the piece.


## Element types

Each element must be of a well defined type. 

### Element type motif

Each type must have a well defined motif: they can only play notes of the configuration, and accidentals (notes outside the configration) are allowed in the motif of an element. 

For example, there may be elements of type 'Carbon' (whatever that means). Elements of type Carbon can only move on a diagonal, maximum 3 notes far. This means that the motif is only 2 notes: the current active note and the target note. Elements of type 'Plastic' must move by 'adjecent' 2 notes, and they may not play the same note twice. This means, from the starting note G3, in the hexagonal configuration above, it can move:
- G3-Bb4-G5
- G3-Bb4-Bb5
- G3-Bb4-Bb2
- G3-Bb2-Bb4
- G3-Bb2-G1
- ...

Any element on one of the notes in the motif will be captured.

### Element type specifics

Each element type is linked to aforementioned base motif. To distinguish between elements, each element can be represented/distinguished by
- multiple instruments, or by one instrument. 
- dynamics
- articulation
- percussion
- ...

The element type motif must be played by the instrument(s). However, if e.g. G3-E4-C5 is the motif to be played, one instrument can play G3-E4 and the other instrument can play E4-C5. If all these notes are played by a number of specific instrument, it is allowed to play e.g. G2-E2-C1. This again, is fixed of what should be played (e.g. same notes, but on the lower scale, played by a bassoon).

### Element jumps

Usually, elements cannot pass through other elements. If an element's motif passes another element, it cannot be played. Another variation of the motif should be played, or another element should play.

However, specific elements may have the trait of jumping. They can jump over other elements towards their target note. They can play their motifs, no matter if there are other elements hindering.

## End condition

There must be an end condition. This can any condition:

- all elements of one group are captured
- an element of a certain type is captured
- an element reaches a certain note
- ...